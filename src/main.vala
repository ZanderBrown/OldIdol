/* main.vala
 *
 * Copyright 2018 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Idol;

class IdolApp : Application {
	private static bool version = false;

	private const GLib.OptionEntry[] options = {
		{ "version", 0, 0, OptionArg.NONE, ref version, "Display version number", null },
		{ "symbolic", 0, 0, OptionArg.NONE, null, "Export symbolic icons", null },
		{ "stable", 0, 0, OptionArg.NONE, null, "Export stable icons", null },
		{ "nightly", 0, 0, OptionArg.NONE, null, "Export nightly icons", null },
		{ OPTION_REMAINING, 0, 0, OptionArg.FILENAME_ARRAY, null, null, "[INPUT] [OUTPUT]" },
		{ null }
	};

	construct {
		application_id = "org.gnome.Idol";
		flags = HANDLES_COMMAND_LINE;

		add_main_option_entries(options);
	}
	
	public override int command_line (ApplicationCommandLine cli) {
		if (version) {
			cli.print("%s %s\n", Environment.get_application_name(), VERSION);
			return 0;
		}
		var dict = cli.get_options_dict();
		var argsv = dict.lookup_value(OPTION_REMAINING, VariantType.BYTESTRING_ARRAY);
		if (argsv == null) {
			cli.printerr("Use --help for usage information\n");
			return 1;
		}
		var args = argsv.get_bytestring_array();
		if (args.length != 2) {
			cli.printerr("Expected 2 arguments\n");
			return 1;
		}
		hold();
		var input = cli.create_file_for_arg(args[0]);
		var output = cli.create_file_for_arg(args[1]);
		var nightly = dict.lookup_value("nightly", VariantType.BOOLEAN);
		var symbolic = dict.lookup_value("symbolic", VariantType.BOOLEAN);

		cli.print("%s -> %s\n", input.get_path(), output.get_path());

		var vec = new Vector();
		vec.load_file.begin(input, (_, r) => {
			try {
				vec.load_file.end(r);
				message(vec.category.to_string());
				if (nightly != null && nightly.get_boolean()) {
					vec.nightly(output);
				} else if (symbolic != null && symbolic.get_boolean()) {
					vec.symbolic(output);
				} else {
					vec.stable(output);
				}
			} catch (Error e) {
				critical(e.message);
			}
			release();
		});
		return 0;
	}
}

int main (string[] args) {
	Environment.set_application_name("Idol");
	return (new IdolApp()).run (args);
}

