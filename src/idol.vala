namespace Idol {
	public enum Category {
		APPLICATION,
		SYMBOLIC,
		BAD
	}

	// Bad name...
	public errordomain IError {
		NOT_SVG
	}

	public class Vector : Object {
		private Rsvg.Handle handle = null;
		private GXml.GomDocument doc = null;

		public Category category { get; private set; }

		private File source = null;

		construct {
		}

		public async void load_file (File file) throws Error {
			uint8[] contents;
			yield file.load_contents_async (null, out contents, null);

			doc = new GXml.GomDocument.from_string ((string) contents);
			var root = doc.query_selector("svg");
			if (root == null) {
				throw new IError.NOT_SVG("Failed to find root element");
			}
			var rect = doc.create_element("rect");
			rect.set_attribute("x", "0");
			rect.set_attribute("y", "244");
			rect.set_attribute("width", "128");
			rect.set_attribute("height", "56");
			rect.set_attribute("style", "fill: url(#__nightly_bar_p); stroke: none;");
			root.append_child(rect);

			var defs = root.query_selector("defs");
			if (defs == null) {
				throw new IError.NOT_SVG("Failed to find defs element");
			}

			var nightlypat = doc.create_element("pattern");
			nightlypat.set_attribute("id", "__nightly_bar_p");
			message("Bo");
			(nightlypat.attributes as GXml.GomElement.Attributes)["xlink:href"] = "#__nightly_bar_p_base";
			message("om");
			nightlypat.set_attribute("patternTransform", "matrix(0.64879356,-0.61884063,1.4632413,1.5340647,-3.8013021e-7,244.00006)");
			defs.append_child(nightlypat);
			message("Un");
			var nightlypatb = doc.create_element("pattern");
			nightlypatb.set_attribute("id", "__nightly_bar_p_base");
			nightlypatb.set_attribute("width", "30");
			nightlypatb.set_attribute("height", "50");
			nightlypatb.set_attribute("patternTransform", "translate(-48.083252,309.39694)");
			nightlypatb.set_attribute("patternUnits", "userSpaceOnUse");
			message("Dos");
			string[] bars = {"fcec00", "000000"};
			var x = 0;
			foreach (var bar in bars) {
				var r = doc.create_element("rect");
				r.set_attribute("x", x.to_string());
				r.set_attribute("y", "0");
				r.set_attribute("width", "15");
				r.set_attribute("height", "50");
				r.set_attribute("style", "opacity:0.7;fill:#%s;fill-opacity:1;".printf(bar));
				nightlypatb.append_child(r);
				x += 15;
			}
			message("Tres");
			defs.append_child(nightlypatb);
/*in defs
<pattern
       xlink:href="#pattern1451"
       id="pattern2173"
       patternTransform="matrix(0.57388844,-0.54739366,1.4021184,1.4699833,-3.8013021e-7,244.00006)" />
<pattern
       patternUnits="userSpaceOnUse"
       width="30"
       height="50"
       patternTransform="translate(-48.083252,309.39694)"
       id="pattern1451">
      <rect
         y="0"
         x="0"
         height="50"
         width="15"
         style="opacity:0.7;fill:#fcec00;fill-opacity:1;" />
      <rect
         y="0"
         x="15"
         height="50"
         width="15"
         style="opacity:0.7;fill:#000000;fill-opacity:1;" />
    </pattern>*/
			source = file;
			handle = new Rsvg.Handle.from_data(contents);
			// Colour (App) icons must be 128 by 128
			if (handle.height == 128 && handle.width == 128) {
				category = APPLICATION;
				// Whereas symbolics are 16 by 16
			} else if (handle.height == 16 && handle.width == 16) {
				category = SYMBOLIC;
				// And anything else is unsupported
			} else {
				category = BAD;
			}
		}

		public void stable (File to) throws Error requires (source != null) {
			message ("Export %s to %s as stable", source.get_path(), to.get_path());
			doc.write_file(to);
		}

		public void nightly (File to) throws Error requires (source != null) {
			message ("Export %s to %s as nightly", source.get_path(), to.get_path());
		}

		public void symbolic (File to) throws Error requires (source != null) {
			message ("Export %s to %s as symbolic", source.get_path(), to.get_path());
		}
	}
}

